# pyloser
(Python Line Of Sight Extinction by Ray-tracing)

For full documentation see readthedocs: http://pyloser.readthedocs.io/en/latest/

Overview: 

pyloser computes galaxy photometry or spectra from hydro simulations, either for individual objects or as a pixelized image, including line of sight extinction to each star particle computed via a kernel integral over the metallicities of intervening gas particles based on a chosen extinction law.  pyloser is a fully native python version of https://bitbucket.org/rthompson/closer, accessing full FSPS functionality through Python-FSPS.  It is built using the following packages: caesar, pygadgetreader, python-fsps, extinction, astropy, h5py, joblib, numpy.  Note that an output catalog file from Caesar is required as an input to pyloser.

Usage:
% python pyloser.py Gadget_snapshot.hdf5 Caesar_catalog.hdf5 parameters Output_file.hdf5 Nproc

An example parameters file is included.

Also, three sample plotting scripts are included:  

-- colormass.py: makes a color-mass diagram from pyloser hdf5 output with pyloser run in 'object' mode; and

-- galimage.py: makes an image from pyloser output run in 'image' mode.

-- galspec.py: plots spectra for some galaxies.


CREDITS: Thanks to Chris Lovell and Desika Narayanan for their patient help.  And a big thanks to Charlie Conroy for FSPS, to Dan Foreman-Mackey, Ben Johnson, and Jonathan Sick for python-FSPS, and to Robert Thompson for Caesar.
