Welcome to pyloser's documentation!
===================================

`pyloser <https://bitbucket.org/romeeld/pyloser>`_ (PYthon Line
Of Sight Extinction by Ray-tracing) is a code to compute galaxy
photometry and spectra from the outputs of cosmological or zoom
hydrodynamic galaxy formation simulations.  Its most novel feature
is that it computes the dust extinction to each star particle
based on the kernel-smoothed line of sight metal column density.
Other than doing an expensive dust radiative transfer calculation using
e.g. `powderday <https://bitbucket.org/desika/powderday>`_ , `pyloser
<https://bitbucket.org/romeeld/pyloser>`_ provides the most accurate
particle-based approach for generating simulated images and spectra
accounting for dust.

`pyloser <https://bitbucket.org/romeeld/pyloser>`_ works off of the
`caesar <https://bitbucket.org/laskalam/caesar>`_ extension to `yt
<http://yt-project.org/>`_, which generates cross-matched galaxy/halo
catalogs.  You must run Caesar on your snapshot first in order
to generate the Caesar catalog for `pyloser
<https://bitbucket.org/romeeld/pyloser>`_.

`pyloser <https://bitbucket.org/romeeld/pyloser>`_ uses the `Flexible
Stellar Population Synthesis (FSPS) <https://github.com/cconroy20/fsps>`_
models from Charlie Conroy, specifically through the `python-fsps
<http://dfm.io/python-fsps/current/>`_ bindings.  All options for
bands, IMFs, emission lines, cosmic absorption, etc available in
FSPS are likewise available in `pyloser
<https://bitbucket.org/romeeld/pyloser>`_.

**To run:**

% python pyloser.py  <snapshot_file>  <Caesar_file>  <Parameter_file>  [Output_file] [nproc]

**Inputs:**

* **Gadget-format hdf5 snapshot**.  Works with all flavors of Gadget and Gizmo.  Be careful about your metallicities!  `pyloser <https://bitbucket.org/romeeld/pyloser>`_ expects metal mass fraction, not solar-scaled, with Ztotal as the first element.

* **Caesar catalog file**.  See Caesar documentation on how to generate this.

* **pyloser parameter file**.  A documented example called "parameters" is on the `bitbucket repo <https://bitbucket.org/romeeld/pyloser>`_.

* **[Output file]**.  Optionally, specify a filename for the output pyloser hdf5 catalog; include the ".hdf5" or ".h5" suffix.  If not specified, appends suffix "_loser.hdf5" to the Caesar file name.

* **[nproc]**.  Optionally, number of cores to use, otherwise uses nproc specified in Parameter_file

**Outputs:**

* An hdf5 file containing the requested photometry and optionally spectra for the requested objects or pixels.  See example scripts below for how to read this file.

**Example scripts:**

These scripts show you examples of how to read in pyloser hdf5 output, along with associated caesar catalog, for some simple plots.

* **colormass.py** shows how to combine pyloser run with 'object'+'phot' and a caesar catalog to generate a color-mass diagram.

* **galimage.py** shows how to use pyloser run with 'image'+'phot' to make an image.

* **galspec.py** shows how to use pyloser run with 'object'+'spec' to plot galaxy spectra.

**Requirements:** 

* `caesar <https://bitbucket.org/laskalam/caesar>`_ -- follow `instructions to install <http://caesar.readthedocs.io/en/latest/getting_started.html>`_

* `pygadgetreader <https://bitbucket.org/rthompson/pygadgetreader>`_ -- follow instructions on bitbucket page to install

* `python-fsps <http://dfm.io/python-fsps/current/>`_ -- follow `instructions to install <http://caesar.readthedocs.io/en/latest/getting_started.html>`_

* `extinction  <https://pypi.python.org/pypi/extinction>`_ -- installs with "pip install extinction"

* `astropy <http://www.astropy.org/>`_ -- installs with "pip install astropy"

* `h5py <https://www.h5py.org/>`_ -- installs with "pip install h5py"

* `joblib <https://pypi.python.org/pypi/joblib>`_ -- installs with "pip install joblib"

* numpy -- um, you have this.

Note that many of the above are also available through Anaconda; try "conda install XXX".

**Parameter File:**

Parameter_file: A text file containing the options passed to pyloser.
An example file called "parameters" is included in the repository.
The parameter list is stored in the output hdf5 file as attributes;
the above example scripts show how to convert it back into a python
dictionary upon reading.

**Objects:** Objects on which pyloser will be run, specified by their
Caesar moniker.  Extinction will only be computed from gas within
those objects.  The objects can be galaxies (sim.galaxies) or halos
(sim.halos), and you can specify a range (sim.galaxies[:10]).  Note that
Caesar orders galaxies by decreasing baryonic mass.

**Extinction:** Choose an extinction law, as described in the file; look
in los_extinction.py for more details.  The parameter "gas_crit" sets the
boolean criteria for gas to qualify as extincting (i.e. containing dust),
specified in terms of gas array names within pyloser.

**Map type**: Can run in "object" mode, i.e.  galaxy-by-galaxy or
halo-by-halo; or "image" mode, i.e. computing quantities in pixels
("image_Npix") centered on a specified location ("image_center") of a
particular size ("image_size").  The image center can be specified as
a Caesar object's position.

**Output type**:  For each object or pixel, this determines whether to
only compute photometry ('phot') or both photometry and spectra ('spec').
In either case you will need to specify a list of bands by their FSPS
moniker; choices can be seen by running python, saying "import fsps",
then "fsps.find_filter('')".  'spec' will compute a spectrum for every
object or pixel, in erg/s/cm^2/AA.  'spec' mode requires a wavelength
range in Angstroms, the number of pixels, and a velocity-space Gaussian
smoothing FWHM (0=no smoothing).

**FSPS options**: There are a number of options passed to the FSPS
SSP generator; the `python-fsps <http://dfm.io/python-fsps/current/>`_
page describes them in more detail.  Not all options from python-fsps
are available in pyloser; if you want one that isn't, it is simple to
add into the source code, or just ask.

* fsps_compute_abs_mag: if True, compute the absolute magnitude independent of the input snapshot redshift.  False means compute apparent magnitudes at that redshift.  A redshift=0 snapshot will automatically compute absolute magnitudes, regardless of this flag.

* fsps_imf_type: Currently only Salpeter, Chabrier, and Kroupa are supported.  FSPS has more options but this requires specifying more parameters.  You could modify the main() routine in pyloser.py if you want to set specific IMF options.

* fsps_add_igm_absorption: Use the Madau prescription for cosmological LOS absorption (if redshift>0).

* fsps_add_neb_emission: Includes nebular emission lines in both photometry and spectra based on the model in FSPS.  This slows the computation considerably.

**Parallelizing**: You can specify "nproc" in the Parameter_file which will be the default if no value is specified on the command line.  Note that pyloser parallelizes over objects (eg galaxies or pixels), so if you are only running pyloser for 5 galaxies it will only use at most 5 cores, even if you specify more.  The value of nproc is interpreted by `joblib <https://pypi.python.org/pypi/joblib>`_: e.g. 4=use 4, -1=use all, -2=use all but 1, etc.


**And lastly...**

Feedback, suggestions for improvements, and (best of all) pull requests are always welcome.  If you find this useful in published work, for now please just footnote this readthedocs page.

Index
=====

* :ref:`genindex`
* :ref:`search`

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. important:: 

   **Disclaimer**: 
   While the developers have made every effort to ensure
   that the code is bug-free, bugs invariably find their
   way in.  The developers are not responsible for
   incorrect results arising either from inherent bugs
   in the code, or from mistakes in documentation or usage.  


