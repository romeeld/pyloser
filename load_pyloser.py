
import numpy as np
import h5py

# return data from pyloser file, either magnitudes or spectra or whatever.
# mtype choices are absmag/appmag/absmag_nodust/appmag_nodust.  also can be spec, iobjs, A_V, or L_FIR (in units of Lsun), in which case bandlist is not needed.
# bandlist is a list of the names of the desired bands, e.g. ['sdss_r','irac_1,'v'].  these must match the band names in the pyloser file.
def get_mags(loserfile,mtype,bandlist=None,verbose=False):
    hf = h5py.File(loserfile,'r')
    # read in pyloser parameters
    p = [i for i in hf.attrs.items()]
    params = dict([(p[i][0],p[i][1]) for i in range(len(p))])
    if verbose: 
        print('Loaded params into dict:',params)
        print('hdf5 file keys: %s' % hf.keys())
    # get desired quantities
    for i in hf.keys():
        if i==mtype: mydata = list(hf[i])
    hf.close()

    # if A_V or L_FIR is requested, these can be directly returned
    if mtype == 'A_V' or mtype == 'L_FIR' or mtype == 'spec' or mtype == 'iobjs': return params,mydata

    # if specific bands are requested, find them in the pyloser file
    bands = params['bands']
    iband1 = iband2 = -1
    magindex = []
    for j in range(len(bandlist)):
        for i in range(len(bands)):
            if bands[i] == bandlist[j]: magindex.append(i)
            #if bands[i].decode('utf-8') == bandlist[j]: magindex.append(i)
    if len(magindex) != len(bandlist):
        print('Bands [',bandlist,'] not all found in pyloser file; available bands=',bands)
        exit()
    mags = []
    # compile desired band magnitudes
    for j in range(len(bandlist)):
        mags.append(np.asarray([m[magindex[j]] for m in mydata]))
    return params,mags

def list_bands(loserfile,verbose=True):
    hf = h5py.File(loserfile,'r')
    # read in pyloser parameters
    p = [i for i in hf.attrs.items()]
    params = dict([(p[i][0],p[i][1]) for i in range(len(p))])
    if verbose: print('Bands:',params['bands'])
    param_names = [i for i in params]
    for i in param_names:
        if i == 'bands': continue
        if verbose: print('%s: %s' % (i,params[i]))
    return params['bands']

# Appends magnitudes from pyloser file lfile2 into lfile1, writes to lfile_out
def append_pyloser(lfile1,lfile2,lfile_out,verbose=True):

    hf1 = h5py.File(lfile1,'r')
    hf2 = h5py.File(lfile2,'r')

    # read in loser parameters
    p1 = [i for i in hf1.attrs.items()]
    params1 = dict([(p1[i][0],p1[i][1]) for i in range(len(p1))])
    p2 = [i for i in hf2.attrs.items()]
    params2 = dict([(p2[i][0],p2[i][1]) for i in range(len(p2))])
    #if verbose: 
    #    print('lfile1 bands:',params1['bands'])
    #    print('lfile2 bands:',params2['bands'])

    # check files can be combined -- everything but bandlist, output_type, spectra stuff must be same 
    if verbose: print('testing that files can be combined...')
    for ip,p in enumerate(p1):
        if 'bands' in p1[ip] or 'output_type' in p1[ip] or 'spec_npix' in p1[ip] or 'spec_wave' in p1[ip]: continue
        assert p1[ip]==p2[ip],'Cannot combine pyloser files -- parameters differ: %s %s'%(p1[ip],p2[ip])
    for iobj,obj in enumerate(hf1['iobjs']):
        assert hf1['iobjs'][iobj]==hf2['iobjs'][iobj],'Cannot combine pyloser files -- objects differ at object number %d'%iobj

    # determine unique set of bands from combined list
    new_ind = np.unique(np.concatenate((params1['bands'],params2['bands'])),return_index=True)[1]
    new_ind = np.sort(new_ind)
    params1['bands'] = np.concatenate((params1['bands'],params2['bands']))[new_ind]
    if verbose: print('new bands list (%d): %s'%(len(params1['bands']),params1['bands']))

    # gather information for output
    bands_wavelength = np.concatenate((hf1['mag_wavelengths'],hf2['mag_wavelengths']))[new_ind]
    mymags = np.concatenate((hf1['absmag'],hf2['absmag']),axis=1)[:,new_ind]
    mymags_app = np.concatenate((hf1['appmag'],hf2['appmag']),axis=1)[:,new_ind]
    mymags_nodust = np.concatenate((hf1['absmag_nodust'],hf2['absmag_nodust']),axis=1)[:,new_ind]
    mymags_app_nodust = np.concatenate((hf1['appmag_nodust'],hf2['appmag_nodust']),axis=1)[:,new_ind]
    iobjs = np.array(hf1['iobjs'])
    A_V = np.array(hf1['A_V'])
    L_FIR = np.array(hf1['L_FIR'])
    spec_wavelengths = np.array(hf1['spec_wavelengths'])
    spec = np.array(hf1['spec'])
    spec_nodust = np.array(hf1['spec_nodust'])
    hf1.close()
    hf2.close()

    with h5py.File(lfile_out, 'w') as hf:
        for key, value in params1.items():
            hf.attrs.create(key, value, dtype=h5py.special_dtype(vlen=str)) 
        hf.create_dataset('iobjs',data=iobjs)
        hf.create_dataset('mag_wavelengths',data=bands_wavelength)
        hf.create_dataset('absmag',data=mymags)
        hf.create_dataset('absmag_nodust',data=mymags_nodust)
        hf.create_dataset('appmag',data=mymags_app)
        hf.create_dataset('appmag_nodust',data=mymags_app_nodust)
        hf.create_dataset('A_V',data=A_V)
        hf.create_dataset('L_FIR',data=L_FIR)
        if params1['output_type'] == 'spec':
            hf.create_dataset('spec_wavelengths',data=spec_wavelengths)
            hf.create_dataset('spec',data=spec)
            hf.create_dataset('spec_nodust',data=spec_nodust)



