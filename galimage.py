

#=======================================================================
# Generates an image in the specified band from a ceasar file and associated pyloser file
#=======================================================================

import caesar
import pylab as plt
import numpy as np
import h5py
import sys

script,caesarfile,loserfile,band1 = sys.argv
#SNAP = int(SNAP)
#caesardir = '/home/rad/data/%s/%s/Groups' % (MODEL,WIND)	# directory for caesar+loser files

def gen_image(loserfile, band1, field='appmag'):
    '''
    input pyloser file and name of the band, generate an image (2D array)
    field can be appmag, appmag_nodust, absmag, absmag_nodust, or even A_V
    '''
    hf = h5py.File(loserfile,'r')

    # read in pyloser parameters
    Npix = eval(hf.attrs['image_Npix'])

    # get magnitudes
    mymags = hf[field][:]
    iobjs = hf['iobjs'][:]

    # find the band
    bands = hf.attrs['bands']
    iband1 = -1
    for i in range(len(bands)):
        if bands[i] == band1: iband1 = i    # this is the index of the band we want
    if iband1 == -1:
        print('Specified band [',band1,'] not found in pyloser file; available bands=',bands)
        return
    print('Doing band',bands[iband1])

    # load our band
    myimage = np.zeros(Npix)
    for i in range(len(iobjs)):
        myimage[iobjs[i][0],iobjs[i][1]] = 10**(-mymags[i,iband1]/2.5)

    hf.close()

    return myimage


if __name__ == '__main__':

    #caesarfile = '%s/%s_%03d.hdf5' % (caesardir,MODEL,SNAP)
    #loserfile = '%s/%s_%03d_loser.hdf5' % (caesardir,MODEL,SNAP)
    sim = caesar.load(caesarfile,LoadHalo=False) # load caesar file

# input pyloser file
    hf = h5py.File(loserfile,'r')
    # read in pyloser parameters
    p = [i for i in hf.attrs.items()]
    params = dict([(p[i][0],p[i][1]) for i in range(len(p))])
    print('Loaded params into dict:',params)
    print('hdf5 file keys: %s' % hf.keys())
    # get required pyloser parameters info
    viewdir = eval(params['view_dir'])
    xdir = (viewdir+1)%3
    ydir = (viewdir+2)%3
    Npix = eval(params['image_Npix'])
    size = eval(params['image_size'])

    # get magnitudes and A_V
    for i in hf.keys():
        if i=='appmag': mymags = list(hf[i])
        #if i=='A_V': A_V = list(hf[i])
        if i=='iobjs': iobjs = list(hf[i])
    # read in bands that were used in pyloser, get indices of desired bands
    bands = params['bands']
    iband1 = -1
    for i in range(len(bands)):
        if bands[i] == band1: iband1 = i	# this is the index of the band we want
    if iband1 == -1:
        print('Specified band [',band1,'] not found in pyloser file; available bands=',bands)
        exit()
    print('Doing band',bands[iband1],'Npix=',Npix,'size=',size)

# load our band
    m1 = np.asarray([m[iband1] for m in mymags])
    myimage = np.zeros(Npix)
    lum = np.zeros(len(iobjs))
    r2 = np.zeros(len(iobjs))
    xave = yave = lumtot = 0.0
    for i in range(len(iobjs)):
        lum[i] = 10**(-m1[i]/2.5)
        myimage[(iobjs[i][0],iobjs[i][1])] = lum[i]
        xave += lum[i]*iobjs[i][0]
        yave += lum[i]*iobjs[i][1]
        lumtot += lum[i]
    xcent = [xave/lumtot,yave/lumtot]  # center of light distribution
    print('centroid at',xcent)
    for i in range(len(iobjs)):
        r2[i] = (iobjs[i][0]-xcent[0])*(iobjs[i][0]-xcent[0])+(iobjs[i][1]-xcent[1])*(iobjs[i][1]-xcent[1])
    sortindex = np.argsort(r2)
    r2 = r2[sortindex]
    lumsum = np.cumsum(lum[sortindex])
    for i in range(len(iobjs)):
        if lumsum[i] > 0.5*lumsum[-1]: 
            rhalflight = np.sqrt(r2[i]) * size[1]/Npix[0]
            print('r_halflight(kpc)=',rhalflight,sim.galaxies[0].radii['stellar_half_mass'])
            break
    plt.imshow(myimage,cmap=plt.cm.ocean_r,extent=[-0.5*size[xdir],0.5*size[xdir],-0.5*size[ydir],0.5*size[ydir]],interpolation='gaussian')
    plt.colorbar()

# plot stuff
    plt.minorticks_on()
    plt.xlim(-25,25)
    plt.ylim(-25,25)
    #plt.xlim(np.log10(mlim)-0.1,)
    #ax.set_ylim(ax.get_ylim()[::-1])	# invert y axis, if desired eg for a mass-magnitude diagram
    plt.ylabel(r'y (kpc)',fontsize=16)
    plt.xlabel(r'x (kpc)',fontsize=16)
    plt.annotate('%s' % bands[iband1], xy=(0.05, 0.05), xycoords='axes fraction',size=16,bbox=dict(boxstyle="round", fc="w"))
    plt.legend(loc='lower right')

    plt.savefig('galimage.pdf', bbox_inches='tight', format='pdf')
    plt.show()


#######################################


