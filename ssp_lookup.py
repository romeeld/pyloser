

#===========================================================================
# ROUTINES TO GENERATE AND USE LOOKUP TABLE INTERPOLATION TO GET SSP SPECTRA
#===========================================================================

import numpy as np
import h5py
from scipy import interpolate
import sys 
import time
from auxloser import t_elapsed

def generate_ssp_table(fsps_ssp,params,Zsol,ssp_lookup_file,TINIT,oversample=[2,2]):
    print('pyloser : Generating SSP lookup table %s: FSPS nwave=%d nage=%d nZ=%d [t=%g s]'%(ssp_lookup_file,len(fsps_ssp.wavelengths),len(fsps_ssp.ssp_ages),len(fsps_ssp.zlegend),t_elapsed(TINIT)))
    mass_remaining = fsps_ssp.stellar_mass
    print('pyloser : Computed mass_remaining vector [t=%g s]'%t_elapsed(TINIT))
    ssp_ages = []
    ssp_ages.append(fsps_ssp.ssp_ages[0])
    for i in range(len(fsps_ssp.ssp_ages)-1):
        for j in range(i+1,i+oversample[0]):
            ssp_ages.append((fsps_ssp.ssp_ages[j]-fsps_ssp.ssp_ages[j-1])*(j-i)/oversample[0]+fsps_ssp.ssp_ages[j-1])
        ssp_ages.append(fsps_ssp.ssp_ages[j])
    ssp_logZ = []
    ssp_logZ.append(fsps_ssp.zlegend[0])
    for i in range(len(fsps_ssp.zlegend)-1):
        for j in range(i+1,i+oversample[1]):
            ssp_logZ.append((fsps_ssp.zlegend[j]-fsps_ssp.zlegend[j-1])*(j-i)/oversample[1]+fsps_ssp.zlegend[j-1])
        ssp_logZ.append(fsps_ssp.zlegend[j])
    ssp_logZ = np.log10(ssp_logZ)
    ssp_spectra = []
    for age in ssp_ages:
        for Zmet in ssp_logZ:
            fsps_ssp.params["logzsol"] = Zmet-np.log10(Zsol)
            spectrum = fsps_ssp.get_spectrum(tage=10**(age-9))[1]
            ssp_spectra.append(spectrum)
    with h5py.File(ssp_lookup_file, 'w') as hf:
        hf.create_dataset('mass_remaining',data=mass_remaining)
        hf.create_dataset('ages',data=ssp_ages)
        hf.create_dataset('logZ',data=ssp_logZ)
        hf.create_dataset('spectra',data=ssp_spectra)
    print('pyloser : Generated lookup table with %d ages and %d metallicities (%d kb) [t=%g s]'%(len(ssp_ages),len(ssp_logZ),int(sys.getsizeof(ssp_spectra)/1.e3),t_elapsed(TINIT)))
    ssp_interpolate = []
    ssp_interpolate.append(ssp_ages)
    ssp_interpolate.append(ssp_logZ)
    ssp_interpolate.append(ssp_spectra)
    return ssp_interpolate,mass_remaining

def read_ssp_table(ssp_lookup_file,TINIT):
    hf = h5py.File(ssp_lookup_file,'r')
    print('pyloser: Reading in pre-computed SSP lookup table file %s [t=%g s]'%(ssp_lookup_file,t_elapsed(TINIT)))
    for i in hf.keys():
        if i=='mass_remaining': mass_remaining = list(hf[i])
        if i=='ages': ssp_ages = list(hf[i])
        if i=='logZ': ssp_logZ = list(hf[i])
        if i=='spectra': ssp_spectra = list(hf[i])
    ssp_interpolate = []
    ssp_interpolate.append(ssp_ages)
    ssp_interpolate.append(ssp_logZ)
    ssp_interpolate.append(ssp_spectra)
    return ssp_interpolate,mass_remaining

def interpolate_ssp_table(fsps_ssp,age,met,ssp_interpolate):  # age in Gyr, met in mass fraction units; note- assumes a uniform grid in ages and metallicities!
    ssp_ages = ssp_interpolate[0]
    ssp_logZ = ssp_interpolate[1]
    ssp_spectra = ssp_interpolate[2]
    logage = np.log10(age)+9
    logZ = np.log10(met)
    logage = np.clip(logage,ssp_ages[0],ssp_ages[-1])
    logZ = np.clip(logZ,ssp_logZ[0],ssp_logZ[-1])
    iage = iZ = 0
    while logage > ssp_ages[iage+1] and iage < len(ssp_ages)-2: iage = iage + 1
    while logZ > ssp_logZ[iZ+1] and iZ < len(ssp_logZ)-2: iZ = iZ + 1
    fage = (logage-ssp_ages[iage])/(ssp_ages[iage+1]-ssp_ages[iage])
    fZ = (logZ-ssp_logZ[iZ])/(ssp_logZ[iZ+1]-ssp_logZ[iZ])
    #print 'age,Z=',logage,logZ,'agerange=',ssp_ages[iage],ssp_ages[iage+1],' fracs=',fage,fZ,' indices=',iage,iZ
    if fage<-1.e-6 or fage>1+1.e-6 or fZ<-1.e-6 or fZ>1+1.e-6: sys.exit('pyloser : interpolation out of bounds: fage=%g fZ=%g'%(fage,fZ))
    i00 = iage*len(ssp_logZ) + iZ
    i01 = iage*len(ssp_logZ) + iZ + 1
    i10 = (iage+1)*len(ssp_logZ) + iZ 
    i11 = (iage+1)*len(ssp_logZ) + iZ + 1
    spectrum = fage*fZ*ssp_spectra[i11] + fage*(1-fZ)*ssp_spectra[i10] + (1-fage)*fZ*ssp_spectra[i01] + (1-fage)*(1-fZ)*ssp_spectra[i00]
    '''
    if True:   # some testing/debugging stuff
        Zsol = 0.0134
        fsps_ssp.params["logzsol"] = logZ-np.log10(Zsol)
        spec_exact = fsps_ssp.get_spectrum(tage=age)[1]
        #print '=========',fage,fZ,logage #,logage-ssp_ages[iage-1],logage-ssp_ages[iage],logZ,logZ-ssp_logZ[iZ-1],logZ-ssp_logZ[iZ]
        print 'interp:',logage,fsps_ssp.params["logzsol"],fage,fZ,np.sum(spectrum-spec_exact)/np.sum(spec_exact)
        #print 'exact:',age,np.log10(age)+9,met,np.log10(met/Zsol)
        with open('error.dat','a') as f:
            if fage<0.9 and fage>0.1 and fZ<0.9 and fZ>0.1: print >> f,'Sums= %g %g Fractional error= %g'%(np.sum(spectrum),np.sum(spec_exact),np.sum(spectrum-spec_exact)/np.sum(spec_exact))
        f.close()
    '''

    return spectrum

#######################################
