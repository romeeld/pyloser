

#=======================================================================
# Generates a color-mass diagram from pyloser hdf5 file (and caesar file)
#=======================================================================

import caesar
import pylab as plt
import numpy as np
import h5py
import sys
from scipy.ndimage import gaussian_filter

script,caesarfile,loserfile = sys.argv # Note: igal must be a list e.g. [5] or [3,7] or [0-9]
#SNAP = int(SNAP)
#caesardir = '/home/rad/data/%s/%s/Groups' % (MODEL,WIND)	# directory for caesar+loser files
qplot = 'HI'

if __name__ == '__main__':

    #caesarfile = '%s/%s_%03d.hdf5' % (caesardir,MODEL,SNAP)
    #loserfile = '%s/%s_%03d_loser.hdf5' % (caesardir,MODEL,SNAP)
    sim = caesar.load(caesarfile) # load caesar file

# input pyloser file
    hf = h5py.File(loserfile,'r')
    # read in pyloser parameters
    p = [i for i in hf.attrs.items()]
    params = dict([(p[i][0],p[i][1]) for i in range(len(p))])
    print('Loaded params into dict: %s'%params)
    print('hdf5 file keys: %s' % hf.keys())
    quants = params['bands']
    print('Found quantities: %s' % quants)
    # set objects that were used in pyloser
    myobjs = eval(params['objs'])
    # get magnitudes and A_V
    for i in hf.keys():
        print i
        if i=='physmap': 
            physmap = list(hf[i])
        if i == qplot:
            print('loading %s %s'%(i,hf[i]))
            physcube = list(hf[i])
    dvel = (eval(params['spec_vel'])[1]-eval(params['spec_vel'])[0])/eval(params['spec_npix'])
    vels = np.arange(eval(params['spec_vel'])[0],eval(params['spec_vel'])[1],dvel)
    qsum = np.array([sum(i) for i in physcube])
    imax = np.argmax(qsum)  # find pixel in image that has the most mass, for plotting
    print imax,np.log10(np.max(qsum)),physcube[imax]

# get caesar info for these objects
#    logms = np.log10(np.asarray([i.masses['stellar'] for i in myobjs]))
#    logmHI = np.log10(np.asarray([i.masses['HI'] for i in myobjs]))

# plot stuff
    spec = gaussian_filter(physcube[imax],2.0)
    fig,ax = plt.subplots()
    plt.plot(vels,physcube[imax],'--',drawstyle='steps')
    plt.plot(vels,spec,'-')

    plt.minorticks_on()
    plt.ylabel(r'$M_{%s}$'%qplot, fontsize=16)
    plt.xlabel(r'$v$ (km/s)' ,fontsize=16)
    plt.annotate(r'$\log\ M=%g$'%np.log10(qsum[imax]), xy=(0.1, 0.9), xycoords='axes fraction',size=14,bbox=dict(boxstyle="round", fc="w"),horizontalalignment='left')
    plt.legend(loc='upper right')

    #plt.savefig('galspec.pdf', bbox_inches='tight', format='pdf')
    plt.show()


#######################################


