'''
Generate parameter file, which will be fed into pyloser to create mock images for a galaxy.

Run this script as:
    python gen_image_params.py caesarfile gal_ind pixelscale [outfile]
caesarfile is the name of the caesar file, e.g. m25n1024_036.hdf5;
gal_ind is the index of the galaxy;
pixelscale is how many arcsec per pixel, e.g. 0.031;
outfile is the name of the parameter file to be written. If not provided, print to screen.

We take the file param_image as a template and substitute some of the parameters in it.
By default, we center the mock images on the center of the galaxy. The size of the images is 4 times
the stellar boundary of the galaxy; if this value results in less than 20 pixels per side, we generate
images of 20x20 pixels.
The filters/bands can be specified in param_image. There are a lot other parameters in that file which 
can be modified. Please see param_image and pyloser documentation for detaills.
'''
import numpy as np
import matplotlib.pyplot as plt
import caesar
from astropy.cosmology import FlatLambdaCDM
import sys

def main():
    caesarfile = sys.argv[1]
    gal_ind = int(sys.argv[2])
    pixelscale = float(sys.argv[3])
    try:
        outfile = sys.argv[4]
    except:
        outfile = None
    
    # load caesarfile
    sim = caesar.load(caesarfile, LoadHalo=False)

    # caculate angular diameter distance
    z = sim.simulation.redshift
    cosmo = FlatLambdaCDM(H0=100*sim.simulation.hubble_constant, Om0=sim.simulation.omega_matter)
    DA = cosmo.angular_diameter_distance(z).to('kpc').value

    # calculate number of pixels and size of image
    pixelscale_in_ckpc = DA*pixelscale*4.84814e-6*(1.+z)
    Npix = max(int(sim.galaxies[gal_ind].radii['stellar']*4/pixelscale_in_ckpc),20)
    image_size = Npix*pixelscale_in_ckpc

    # load template parameter file
    with open('param_image', 'r') as f:
        script = f.read()

    # output parameter file
    if outfile is not None:
        with open(outfile, 'w') as f:
            f.write(script.format(gal_ind=gal_ind, size1=image_size, size2=image_size, Npix1=Npix, Npix2=Npix))
    else:
        print(script.format(gal_ind=gal_ind, size1=image_size, size2=image_size, Npix1=Npix, Npix2=Npix))

if __name__ == '__main__':
    main()

