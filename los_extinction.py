
import numpy as np
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from scipy import spatial
import time
import os
import sys
import extinction


#=========================================================
# GLOBAL DEFINITIONS
#=========================================================

NKERNTAB = 1000         # entries in kernel interpolation table
AVFLOOR = 0.0           # minimum value of A_V

#=========================================================
# AUXILIARY ROUTINES
#=========================================================

def set_solar():
    Solar = []
    Solar.append(0.0134) # 0=all metals (by mass); present photospheric abundances from Asplund et al. 2009 (Z=0.0134, proto-solar=0.0142) in notes;
    Solar.append(0.2485) # 1=He  (10.93 in units where log[H]=12, so photospheric mass fraction -> Y=0.2485 [Hydrogen X=0.7381]; Anders+Grevesse Y=0.2485, X=0.7314)
    Solar.append(2.38e-3) # 2=C   (8.43 -> 2.38e-3, AG=3.18e-3)
    Solar.append(0.70e-3) # 3=N   (7.83 -> 0.70e-3, AG=1.15e-3)
    Solar.append(5.79e-3) # 4=O   (8.69 -> 5.79e-3, AG=9.97e-3)
    Solar.append(1.26e-3) # 5=Ne  (7.93 -> 1.26e-3, AG=1.72e-3)
    Solar.append(7.14e-4) # 6=Mg  (7.60 -> 7.14e-4, AG=6.75e-4)
    Solar.append(6.71e-4) # 7=Si  (7.51 -> 6.71e-4, AG=7.30e-4)
    Solar.append(3.12e-4) # 8=S   (7.12 -> 3.12e-4, AG=3.80e-4)
    Solar.append(0.65e-4) # 9=Ca  (6.34 -> 0.65e-4, AG=0.67e-4)
    Solar.append(1.31e-3) # 10=Fe (7.50 -> 1.31e-3, AG=1.92e-3)
    return np.asarray(Solar)

def cosmic_extinction(lam,redshift):  # Madau (1995) prescription for IGM absorption
    t_eff = 0.
    l_a = 1216.
    l_b = 1026.
    l_limit = 912.
    xc = 0.
    xem = 1. + redshift
    l_cutoff = 600*(1.+redshift) # Madau fails for low lambda; assume complete absorption 

    lam_ = np.array([lam]).reshape(-1)
    t_eff = np.zeros_like(lam_)
    # lambda is between rest-frame lyman beta and lyman alpha 
    ii = (l_b*(1.+redshift) < lam_) & (l_a*(1.+redshift) > lam_)
    t_eff[ii] = 0.0036 * np.power(lam_[ii]/l_a,3.46) + 0.0017 * np.power(lam_[ii]/l_a,1.68)
    # lambda is blueward of rest-frame lyman beta
    ii = (lam_ <= l_b*(1.+redshift))
    # but not necessarily Lyman limit
    t_eff[ii] = 0.0036*np.power(lam_[ii]/1216.,3.46) + 0.0017*np.power(lam_[ii]/1026,3.46) + 0.0012*np.power(lam_[ii]/973,3.46) + 0.00093*np.power(lam_[ii]/950,3.46)
    # blueward of Lyman limit--include continuum absorption
    ii = (lam_ < l_limit*(1.+redshift))
    xc = lam_[ii]/l_limit
    t_eff[ii] += 0.25*np.power(xc,3.)*(np.power(xem,0.46)-np.power(xc,0.46)) + 9.4*np.power(xc,1.5)*(np.power(xem,0.18)-np.power(xc,0.18)) - 0.7*np.power(xc,3.)*(np.power(xc,-1.32)-np.power(xem,-1.32)) - 0.0023*(np.power(xem,1.68)-np.power(xc,1.68))

    ext = np.exp(-t_eff)
    # complete absorption
    ext[lam_ <= l_cutoff] = 0.
    # no absorption
    ext[lam_ >= l_a*(1.+redshift)] = 1.0
    
    if len(lam_) == 1: return ext[0]
    return ext

#=========================================================
# ROUTINES TO DO LOS DUST EXTINCTION
#=========================================================

# set up a list of various extinction laws we might need
def setup_extinction(wave):
    ext_laws = []
    ext_laws.append(extinction.calzetti00(wave, 1.0, 4.05))  # atten_laws[0]
    ext_laws.append(extinction.fm07(wave, 1.0))  # atten_laws[1]
    return ext_laws

# use extinction package (http://extinction.readthedocs.io/en/latest/) to compute extinction
def apply_extinction(ext_law,A_V,starage,ssfr_fact,wave,atten_laws):
    if A_V < 0.01: return np.ones(len(wave))
    if ext_law=='ccm89': A_lambda =  extinction.ccm89(wave, A_V, 3.1)
    #if ext_law=='ccm89': A_lambda =  galactic_extinction(wave, A_V, 3.1)
    elif ext_law=='calzetti00': A_lambda = A_V * atten_laws[0]  # Calzetti+00 starburst
    elif ext_law=='fm07': A_lambda =  A_V * atten_laws[1]  # Fitzpatrick+Massa 07 Milky Way
    elif ext_law=='salmon16':   # Noll+09: Calzetti plus power law in lambda, as fit by Salmon+16
        A_lambda =  A_V * atten_laws[0]
        delta = 0.62*np.log10(A_V/4.05)+0.26
        A_lambda *= np.power(wave/5500,-delta)
        A_lambda = np.clip(A_lambda,0.,25.)
    elif ext_law=='cf00_calzetti':  # Charlot+Fall 2000 Fig 5: Calzetti + extra power law for young ages
        A_lambda =  A_V * atten_laws[0]
        logage = np.log10(starage*1.e9)
        if logage < 7.5:
            for i in range(len(wave)):
                if wave[i] < 5000:
                    delta = 0.15*(7.5-logage)/0.7        # eyeball fit to tau vs. wave at t<3e7 from CF00 fig 5
                    A_lambda[i] *= np.exp(-np.power(wave[i]/5000,-delta))
    elif ext_law=='mix_calz00_fm07':  # Calzetti for high-sSFR (>0.3 Gyr^-1), FM07 for others
        #A_lambda = extinction.calzetti00(wave, A_V, 4.05)*ssfr_fact + extinction.fm07(wave, A_V)*(1.-ssfr_fact)
        A_lambda = A_V * (atten_laws[0]*ssfr_fact + atten_laws[1]*(1.-ssfr_fact))
    else: 
        print("I'm a loser, baby! I don't recognize extinction law",ext_law)
        exit()
    return np.power(10,-0.4*A_lambda)

def compute_AV(idir,gmass,gpos,ghsm,gmet,spos,Solar,Lbox,ascale,ktype,kerntab,lowZ_scaling='Simba_DTM'):
    A_V = np.zeros(len(spos))
    if len(gmass) == 0: return A_V   # no extincting gas in this object

    idir1 = (idir+1)%3
    idir2 = (idir+2)%3
    NHcol_fact = 1.99e33*0.76/(3.086e21**2*1.673e-24*ascale*ascale)
    AV_fact = 1./(2.2e21*0.0189)
    redshift = 1./ascale-1.
    dtm_MW = 0.4/0.6  # dust-to-(total)metal ratio in MW = 40% (Dwek 1998, Watson 2012)
    for i in range(len(spos)):
        starpos = spos[i]
        # only consider gas in front of star along LOS
        ii = np.asarray(gpos)[:,idir] >= starpos[idir]
        gp = np.asarray(gpos)[ii]
        gm = np.asarray(gmass)[ii]
        gZ = np.asarray(gmet)[ii]  # remember, for gas_metal_index=-1 this actually holds dust mass fraction
        gh = np.asarray(ghsm)[ii]

        # select gas particles whose smoothing length can cover the star
        dy = abs(gp[:,idir1]-starpos[idir1])
        ii = dy > 0.5*Lbox
        dy[ii] = Lbox - dy[ii]
        dz = abs(gp[:,idir2]-starpos[idir2])
        ii = dz > 0.5*Lbox
        dz[ii] = Lbox - dz[ii]
        ds2 = dy**2 + dz**2
        ii = ds2 <= gh**2
        
        Zave = np.sum(gm[ii]*gZ[ii])
        mgtot = np.sum(gm[ii])
        boverh = np.sqrt(ds2[ii])/gh[ii]
        kernint_val = np.array([kerntab[int(NKERNTAB*x)] for x in boverh])
        NHcol = NHcol_fact*np.sum(gm[ii]*gZ[ii]/(gh[ii]*gh[ii])*kernint_val)

        # get A_V from N_H:  Use MW scaling, plus a correction for low-Z systems from high-z GRBs
        A_V[i] = NHcol*AV_fact #  Watson 2011 arXiv:1107.6031 (note: Watson calibrates to solar_abund=0.0189)
        if Zave>0 and A_V[i]>0:      #  use average Z to adjust dust-to-metal ratio
            Zave /= (mgtot*Solar[0])
            # Apply a formula to drop the dust extinction at low metallicity if desired
            if lowZ_scaling == 'DeCia13':
                if Zave<1.0: A_V[i] *= (1.+0.3*np.log10(Zave))  # by-eye fit to De Cia+13 from GRBs
            elif lowZ_scaling == 'Simba_DTM':
                # z- and Z-dependent fit to Simba-100 dust-to-metal ratio: plot_dtm_hexbin.py
                dtm_slope = -0.104*redshift + 0.97  # slope at a given redshift
                dtm_int = -0.059*redshift + 0.005   # intercept at a given redshift
                dtm = 10**(dtm_slope*np.log10(Zave) + dtm_int)  # dtm value at given metallicity
                if dtm<dtm_MW: A_V[i] *= dtm/dtm_MW  # scale dust extinction to MW dtm value
                #if Zave<1.0: A_V[i] *= 10**(0.5*np.log10(Zave)-0.25) # by-eye fit to Simba's z=3 DTM
        if A_V[i]<AVFLOOR: A_V[i]=AVFLOOR
    return A_V


#=========================================================
# ROUTINES TO SET UP LOS KERNEL INTEGRAL
#=========================================================

'''
Here we tabulate LOS kernel integral as fcn of b/h.
Derivation of LOS kernel integral:
Column density through the kernel W is NH = fH m/mH Int[W(l)dl]
where l = sqrt(b^2+x^2), fH=0.76, mH=Hyd mass, and m=particle's mass.
Thus dl = xdx / sqrt(b^2+x^2)
        = h x'dx'/sqrt(b'^2+x'^2).
where x'= x/h and b'=b/h.
kerntab[] tabulates Int[W x'dx'/sqrt(b'^2+x'^2)] for various b'=b/h.
Then you have to multiply by h, as well as fH and m, to get NH.
Note: W in kernel() requires dividing by h^3, so you end up dividing by h^2.
'''

def kernel(q,ktype):
    if ktype=='cubic':
        if q < 0.5: return (2.546479089470 + 15.278874536822 * (q - 1) * q * q)
        elif q < 1: return 5.092958178941 * (1.0 - q) * (1.0 - q) * (1.0 - q)
        else: return 0
    elif ktype=='quintic':
        if q<0.333333333: return 27.0*(6.4457752*q*q*q*q*(1.0-q) -1.4323945*q*q +0.17507044)
        elif q<0.666666667: return 27.0*(3.2228876*q*q*q*q*(q-3.0) +10.7429587*q*q*q -5.01338071*q*q +0.5968310366*q +0.1352817016)
        elif q<1: return 27.0*0.64457752*(-q*q*q*q*q +5.0*q*q*q*q -10.0*q*q*q +10.0*q*q -5.0*q +1.0)
        else: return 0
    else:
        print("I'm a loser, baby! I don't recognize kernel_type",ktype)
        exit()

# Generate kernel integral lookup table.  Note that this assumes the kernel extent is 1.0*hsmooth.
def init_kerntab(ktype):
    kerntab = np.zeros(NKERNTAB+1)
    binsize = 1./NKERNTAB
    for i in range(0,NKERNTAB+1):
        b = i*binsize
        kerntab[i] = 0.0
        for j in range(0,NKERNTAB+1):
            x = j*binsize
            xnext = x+binsize
            sqrtl = np.sqrt(x*x+b*b)
            sqrtlnext = np.sqrt(xnext*xnext+b*b)
            if i>0: kerntab[i] += 0.5*binsize*(x*kernel(sqrtl,ktype)/sqrtl+xnext*kernel(sqrtlnext,ktype)/sqrtlnext)
            else: kerntab[i] += 0.5*binsize*(kernel(sqrtl,ktype)+kernel(sqrtlnext,ktype))
        kerntab[i] *= 2.0       # above calculates integral in half the kernel, so must double
    return kerntab

